﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginScreen : MonoBehaviour
{
    [SerializeField] private ScreenController _screenController;
    [SerializeField] private static string AccessCode = "Ellipt@2020";
    [SerializeField] private Text StatusMessageTxt;
    [SerializeField] private InputField AccessCodeInputField;
    [SerializeField] private Text AccessCodeErrMsg;


    // Start is called before the first frame update
    void Start()
    {

        if (PlayerPrefs.HasKey("AccessCode"))
        {
            AccessCodeInputField.text = PlayerPrefs.GetString("AccessCode");
            //ScreenManager.Instance.Show("ProfileScreen");
            //Debug.Log("Access Code ..." + PlayerPrefs.GetString("AccessCode"));
        }
    }

    public void OnSubmitAccessCode()
    {
        if (AccessCode != AccessCodeInputField.text)
        {
            StatusMessageTxt.gameObject.SetActive(true);
            AccessCodeErrMsg.gameObject.SetActive(true);
        }
        else
        {
            StatusMessageTxt.gameObject.SetActive(false);
            PlayerPrefs.SetString("AccessCode", AccessCodeInputField.text);
            ScreenManager.Instance.Show("DisclaimerScreen");
            AccessCodeErrMsg.gameObject.SetActive(false);
        }
    }

    
}
