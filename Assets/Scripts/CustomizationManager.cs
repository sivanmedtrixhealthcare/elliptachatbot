﻿using System.Collections;
using System.Collections.Generic;
using BayatGames.SaveGamePro;
using UnityEngine;
using UnityEngine.UI;

public class CustomizationManager : MonoBehaviour
{
    /*enum CustomItems
    {
        HAIR,
        ASSET,
        DRESS
    } */

    [SerializeField] private GameObject maleItems;
    [SerializeField] private GameObject femaleItems;
    [SerializeField] private GameObject[] maleCustomIcons;
    [SerializeField] private GameObject[] femaleCustomIcons;

    [SerializeField] private GameObject[] maleHairModels;
    [SerializeField] private GameObject[] maleAssetModels;
    [SerializeField] private GameObject[] maleDressModels;

    [SerializeField] private GameObject[] femaleHairModels;
    [SerializeField] private GameObject[] femaleAssetModels;
    [SerializeField] private GameObject[] femaleDressModels;

    [SerializeField] private RectTransform CharacterRenderTexture;
    [SerializeField] private ScreenController _screenController;

    private Dictionary<string, int> appearanceDetails = new Dictionary<string, int>();
    //private const string CUSTOM_CHARACTER_IDENTIFIER = "CustomCharacterData.dat";
    private bool isAtStart = true;

    [SerializeField] private Sprite[] customBGs;
    [SerializeField] private Image whiteBGHolder;
    private int bgCount = 0;

    [SerializeField] private GameObject maleAvatarSwitchIcon;
    [SerializeField] private GameObject femaleAvatarSwitchIcon;
    [SerializeField] private GameObject AvatarSwitchIcon;

    [SerializeField] private Text ScreenTitle;  



    void Awake()
    {
        _screenController.OnShowStarted += ShowStarted;
        _screenController.OnHideStarted += HideStarted;
    }
     

    private void ShowStarted()
    {
        StartDisplay();

        AppManager.Instance.BottomPanel.SetActive(false);
        AppManager.Instance.Backbutton.SetActive(false);
        AppManager.Instance.CustomCharBottomPanel.SetActive(true);
        CharacterRenderTexture.transform.gameObject.SetActive(true);
        OnBGArrowsClicked("RightArrow");
        AvatarSwitchIcon.SetActive(true);
        if(AppManager.Instance.currentAvatarType == AvatarType.MALE)
            ScreenTitle.text = "Ell";
        else
            ScreenTitle.text = "Elli";
    }

    private void HideStarted()
    {
        maleAvatarSwitchIcon.SetActive(false);
        femaleAvatarSwitchIcon.SetActive(false);
    }


    private void StartDisplay()
    {
        if (AppManager.Instance.currentAvatarType == AvatarType.MALE)
        {

            AppManager.Instance.modelCamera.transform.position = new Vector3(0, 1.1f, -4f);
            maleAvatarSwitchIcon.SetActive(false);
            femaleAvatarSwitchIcon.SetActive(true);
        }
        else if (AppManager.Instance.currentAvatarType == AvatarType.FEMALE)
        {
            AppManager.Instance.modelCamera.transform.position = new Vector3(2, 1.1f, -4f);
            maleAvatarSwitchIcon.SetActive(true);
            femaleAvatarSwitchIcon.SetActive(false);
        }
        AppManager.Instance.modelCamera.orthographicSize = 0.8f;
        OnCustomTypeClicked("Hair");


        if (SaveGame.Exists(AppManager.CUSTOM_CHARACTER_IDENTIFIER))
        {
            appearanceDetails = SaveGame.Load<Dictionary<string, int>>(AppManager.CUSTOM_CHARACTER_IDENTIFIER);
            Debug.Log("LOADED CUSTOM CHARACTER DATA :>"+ appearanceDetails);

            foreach (KeyValuePair<string, int> dictValue in appearanceDetails)
            {
                string savedCharacterItems = (dictValue.Key+ "_" + dictValue.Value);
                OnCustomItemClicked(savedCharacterItems);
                //Debug.Log("RETREIVED KEY AND VALUE ::::::::" + dictValue.Key + " VALUE: " + dictValue.Value + " savedChar " + savedCharacterItems); //  
            }
        }
        isAtStart = false;
    }

    public void OnCustomItemClicked(string clickedItemName)
    { 
        Debug.Log("clickedItemName  ::...."+ clickedItemName); // .Split('_')[0]
        string itemName = clickedItemName.Split('_')[0];
        int itemID  = int.Parse(clickedItemName.Split('_')[1]);
        switch (itemName)
        {
            case "MaleHairIcon":
                ChangeCharacterItem(maleHairModels, itemID);
                break;

            case "MaleAssetIcon":
                ChangeCharacterItem(maleAssetModels, itemID);
                break;

            case "MaleDressIcon":
                ChangeCharacterItem(maleDressModels, itemID);
                break;

            case "FemaleHairIcon":
                ChangeCharacterItem(femaleHairModels, itemID);
                break;

            case "FemaleAssetIcon":
                ChangeCharacterItem(femaleAssetModels, itemID);
                break;

            case "FemaleDressIcon":
                ChangeCharacterItem(femaleDressModels, itemID);
                break;
        }

        if (isAtStart == true) return;
        //Debug.Log("Coming after this is at start ...................");
        if (!appearanceDetails.ContainsKey(itemName))
            appearanceDetails.Add(itemName, itemID);
        else
            appearanceDetails[itemName] = itemID;

    }

    private void ChangeCharacterItem(GameObject[] CharItemArray, int CharItemID)
    {
        for(int i = 0; i < CharItemArray.Length; i++)
        {
            if (i == CharItemID)
                CharItemArray[i].SetActive(true);
            else
                CharItemArray[i].SetActive(false);
        }
    }


    public void OnCustomTypeClicked(string clickedTypeName)
    {
        //Debug.Log("clickedTypeName  ::...." + clickedTypeName);
         
        if (AppManager.Instance.currentAvatarType == AvatarType.MALE)
        {

            switch (clickedTypeName)
            {
                case "Hair":
                    HideMaleRemainingIcons("MaleHairIcons");
                    break;

                case "Asset":
                    HideMaleRemainingIcons("MaleAssetIcons");
                    break;

                case "Dress":
                    HideMaleRemainingIcons("MaleDressIcons");
                    break;
            }
            maleItems.SetActive(true);
            femaleItems.SetActive(false);

        }
        else if (AppManager.Instance.currentAvatarType == AvatarType.FEMALE)
        {
            switch (clickedTypeName)
            {
                case "Hair":
                    HideFemaleRemainingIcons("FemaleHairIcons");
                    break;

                case "Asset":
                    HideFemaleRemainingIcons("FemaleAssetIcons");
                    break;

                case "Dress":
                    HideFemaleRemainingIcons("FemaleDressIcons");
                    break;
            }

            femaleItems.SetActive(true);
            maleItems.SetActive(false);
        }
         
    }
     
    private void HideMaleRemainingIcons(string currentSelectedIcons)
    {
        for(int i =0; i< maleCustomIcons.Length; i++)
        {
            if(currentSelectedIcons == maleCustomIcons[i].name)
                maleCustomIcons[i].SetActive(true);
            else
                maleCustomIcons[i].SetActive(false);
        }
    }

    private void HideFemaleRemainingIcons(string currentSelectedIcons)
    {
        for (int i = 0; i < femaleCustomIcons.Length; i++)
        {
            if (currentSelectedIcons == femaleCustomIcons[i].name)
                femaleCustomIcons[i].SetActive(true);
            else
                femaleCustomIcons[i].SetActive(false);
        }
    }
     

    public void OnSaveCustomItems()
    {
        SaveGame.Save(AppManager.CUSTOM_CHARACTER_IDENTIFIER, appearanceDetails);
        AppManager.Instance.CustomCharBottomPanel.SetActive(false);
        ScreenManager.Instance.Show("ConditionMenuScreen");
        //Debug.Log("SAVED DATA IN DICTONARY .>>>"+ appearanceDetails);

    }


    public void OnBGArrowsClicked(string arrowName)
    {
        switch(arrowName)
        {

            case "LeftArrow":
                if (bgCount > 1)
                {
                    //Debug.Log("arrowName :>>" + arrowName + " bgCount : " + bgCount + " customBGs :" + customBGs.Length);
                    bgCount--;
                    whiteBGHolder.GetComponent<Image>().sprite = customBGs[bgCount];
                }
                break;

            case "RightArrow":
                if(bgCount < customBGs.Length)
                {
                    //Debug.Log("arrowName :>>" + arrowName + " bgCount : " + bgCount + " customBGs :" + customBGs.Length);
                    whiteBGHolder.GetComponent<Image>().sprite = customBGs[bgCount];
                    bgCount++;
                }

                break;
        }
     }

    public void OnSwitchAvatarClicked()
    {
        if(AppManager.Instance.currentAvatarType == AvatarType.MALE)
        {
            maleAvatarSwitchIcon.SetActive(true);
            femaleAvatarSwitchIcon.SetActive(false);
            AppManager.Instance.modelCamera.transform.position = new Vector3(2, 1.1f, -4f);
            AppManager.Instance.currentAvatarType = AvatarType.FEMALE;
            AppManager.Instance.ScreenTitle.text = "Elli";
        }
        else if (AppManager.Instance.currentAvatarType == AvatarType.FEMALE)
        {
            maleAvatarSwitchIcon.SetActive(false);
            femaleAvatarSwitchIcon.SetActive(true);
            AppManager.Instance.modelCamera.transform.position = new Vector3(0, 1.1f, -4f);
            AppManager.Instance.currentAvatarType = AvatarType.MALE;
            AppManager.Instance.ScreenTitle.text = "Ell";

        }

        AppManager.Instance.modelCamera.orthographicSize = 0.8f;
        OnCustomTypeClicked("Hair");
    }


}
