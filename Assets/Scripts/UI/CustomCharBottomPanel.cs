﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomCharBottomPanel : MonoBehaviour
{
	/*[SerializeField] private GameObject hair;
	[SerializeField] private GameObject asset;
	[SerializeField] private GameObject dress;
	[SerializeField] private GameObject background;*/
    [SerializeField] private GameObject selectedMark;

    [SerializeField] private Sprite[] activeIcons;
    [SerializeField] private Sprite[] inActiveIcons;
    [SerializeField] private GameObject[] characterObjectIcons;


    [SerializeField] private Button leftArrowBtn;
    [SerializeField] private Button rightArrowBtn;
     
 
    public void OnCustomTypeClicked(string clickedTypeName)
	{
        string charIconIdname = clickedTypeName.Split('_')[1].ToString();
        int clickedID = int.Parse(charIconIdname);
        Debug.Log("clickedTypeName on Custom CharBottom Panel..  ::...." + clickedTypeName);


        //if (AppManager.Instance.currentAvatarType == AvatarType.MALE)
        //{

        switch (clickedID)
			{
				case 0:
                    selectedMark.transform.SetParent(characterObjectIcons[0].transform,false);
                    leftArrowBtn.gameObject.SetActive(false);
                    rightArrowBtn.gameObject.SetActive(false);
                    break;

				case 1:
                    selectedMark.transform.SetParent(characterObjectIcons[1].transform,false);
                    leftArrowBtn.gameObject.SetActive(false);
                    rightArrowBtn.gameObject.SetActive(false);
                    break;

				case 2:
                    selectedMark.transform.SetParent(characterObjectIcons[2].transform, false);
                    leftArrowBtn.gameObject.SetActive(false);
                    rightArrowBtn.gameObject.SetActive(false);
                    break;

                case 3:
                    selectedMark.transform.SetParent(characterObjectIcons[3].transform, false);
                    leftArrowBtn.gameObject.SetActive(true);
                    rightArrowBtn.gameObject.SetActive(true);
                    break;
            }

        //}
        ChangeRemainingIconState(clickedID);
    }

    private void ChangeRemainingIconState(int clickedID)
    {
        for(int i=0; i < characterObjectIcons.Length;i++)
        {
            if (i == clickedID)
                characterObjectIcons[i].GetComponent<Image>().sprite = activeIcons[i];
            else
                characterObjectIcons[i].GetComponent<Image>().sprite = inActiveIcons[i];
        }
    }
}
