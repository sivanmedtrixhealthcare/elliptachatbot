﻿using System;
using System.Collections.Generic;
using App.Data.Reminders;
using QuickEngine.Extensions;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class PickerCell : MonoBehaviour
{
    public enum CellState
    {
        Inactive,
        Active,
        Selected
    }

    [SerializeField] private GameObject[] _remindersIcons;
    [SerializeField] private Image _selectedBgImage;
    [SerializeField] private GameObject _remindersIconsContainer;

    public DateTime dateTime = DateTime.MinValue;
    public Image image;
    public Text text;
    public Button button;
    public CellState state;
    public CellClicked cellClickedDelegate;

    [HideInInspector] public List<ReminderData> reminders;

    public delegate void CellClicked(PickerCell cell);

    public void ClickHandler()
    {
        //Debug.Log("PICKER CELL  CLICKED DATE.... : ");
        cellClickedDelegate?.Invoke(this);
    }
 
    public void SetDate(DateTime dateTime, CellState state, List<ReminderData> reminders)
    {
        this.reminders = reminders;
        this.state = state;
        this.dateTime = dateTime;
        //Debug.Log("PICKER CELL SET  DATE.... : ");

        UpdateView();
    }

    private void UpdateView()
    {
        //Debug.Log("PICKER CELL UPDATE VIEW .... : ");

        _selectedBgImage.enabled = false;
        _remindersIconsContainer.SetActive(false);

        if (state == CellState.Inactive)
        {
            text.fontStyle = FontStyle.Normal;
            _remindersIconsContainer.SetActive(true);
        }
        else if (state == CellState.Active)
        {
            text.fontStyle = FontStyle.Bold;
            _remindersIconsContainer.SetActive(true);
        }
        else if (state == CellState.Selected)
        {
            text.fontStyle = FontStyle.Bold;
            _selectedBgImage.enabled = true;
        }

        
        // show all notification even passed or deleted
        for (int i = 0; i < reminders.Count; i++)
        {
            if(reminders[i].id == ReminderManager._DEFAULT_SAA_ST_KEY)
            {
                _remindersIcons[0].SetActive(true);
     
            }
            else if (reminders[i].id == ReminderManager._DEFAULT_SAA_ACT_KEY)
            {
                _remindersIcons[1].SetActive(true);
   
            }
            else if(reminders[i].id != ReminderManager._DEFAULT_SAA_ACT_KEY && reminders[i].id != ReminderManager._DEFAULT_SAA_ST_KEY)
            {
                _remindersIcons[2].SetActive(true);
            }
            else
            {
                _remindersIcons[2].SetActive(false);
                _remindersIcons[1].SetActive(false);
                _remindersIcons[0].SetActive(false);
            }

            //_remindersIcons[i].SetActive(i < this.reminders.Count);
        }
    }
}