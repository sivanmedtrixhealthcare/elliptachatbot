﻿using UnityEngine;
using App.Data.SSA;
using System;
//using MaterialUI;

public class AsthmaControlTrackerScreen : TrackerScreen
{
    public override void Start()
    {
        Debug.Log("Coming to Start asthma control ....");
        _trackerType = TrackerManager.TrackerType.Asthma;
        //StartTracker();
        base.Start();
    }

    public override void StartTracker()
    {
        Debug.Log("Coming to Start Tracker ....");
        // create new data, because now we don't need to modify existing data until it's been submitted by user in a last step
        _trackerData = new AsthmaData(DateTime.Today);

        _QAPanel.gameObject.SetActive(true);
        _ProgressPanel.transform.gameObject.SetActive(true);

        ScreenManager.Instance.Show("ACT_Test_Screen");
        base.StartTracker();
    }
    
    public override void SubmitResults()
    {
        base.SubmitResults();

        // Need to Change this functionality.
        _QAPanel.gameObject.SetActive(false);
        _ProgressPanel.transform.gameObject.SetActive(false);

        ScreenManager.Instance.Show("AsthmaTrackerMenu");
        //ScreenControllerManager.Instance.Set(4);
    }

    public override void SubmitAnswer()
    {
        base.SubmitAnswer();
         
        if (_trackerData != null)
        {
            if (_trackerData.GetScore() >= 1)
            {
                /*if (!AppManager.Saanotfn.Contains("PLEASE VISIT A PHYSICIAN"))
                {
                    AppManager.Saanotfn.Add("PLEASE VISIT A PHYSICIAN");
                } */
            }
        }
    }


    // Need to rewrite this functionality somewhere. written for time being.
    public override void RedoTest()
    {
        _questionText.gameObject.SetActive(true);
        _answersContainer.transform.gameObject.SetActive(true);

        base.RedoTest();
    }

}
