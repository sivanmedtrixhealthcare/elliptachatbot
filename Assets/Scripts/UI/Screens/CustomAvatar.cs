﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DoozyUI;

public class CustomAvatar : MonoBehaviour
{
    [SerializeField]private Toggle MaleToggle;
    [SerializeField] private Toggle GirlToggle;
    [SerializeField] private ScreenController _screenController;

    [SerializeField] private GameObject MaleAvatar;
    [SerializeField] private GameObject FemaleAvatar;
    [SerializeField] private RectTransform CharacterRenderTexture;

    [SerializeField] private Sprite customBG;
    [SerializeField] private Image whiteBGHolder;

    // Start is called before the first frame update
    void Awake()
    {
        _screenController.OnShowStarted += ShowStarted;
        _screenController.OnHideStarted += HideStarted;

    }
    private void ShowStarted()
    {
        AppManager.Instance.BottomPanel.SetActive(false);
        AppManager.Instance.Backbutton.SetActive(false);
        AppManager.Instance.ScreenTitle.text = "CHOOSE YOUR ASSISTANT";
        AppManager.Instance.modelCamera.transform.position = new Vector3(0, 0.77f, -4f);
        AppManager.Instance.modelCamera.orthographicSize = 1.26f;
        MaleAvatar.transform.position = new Vector3(-0.44f,-0.08f,0f);
        FemaleAvatar.transform.position = new Vector3(0.44f,0f,0f);
        CharacterRenderTexture.transform.gameObject.SetActive(true);
        whiteBGHolder.GetComponent<Image>().sprite = customBG;

    }

    private void HideStarted()
    {
        //AppManager.Instance.modelCamera.orthographicSize = 1.0f;
        MaleAvatar.transform.position = new Vector3(0f, 0f, 0f);
        FemaleAvatar.transform.position = new Vector3(2f, 0f, 0f);
        //CharacterRenderTexture.transform.gameObject.SetActive(false);
    }

    public void OnMaleAvatarSelected(string selectedAwatar)
	{
 
        if (MaleToggle.isOn)
        {
            //Debug.Log("SELECTED AVATAR is Male :>>>" + selectedAwatar);
            AppManager.Instance.currentAvatarType = AvatarType.MALE;

        }
        ShowConfirmPopup();

    }
    public void OnFemaleAvatarSelected(string selectedAwatar)
    {
         
        if (GirlToggle.isOn)
        {
            //Debug.Log("SELECTED AVATAR is Girl :>>>" + selectedAwatar);
            AppManager.Instance.currentAvatarType = AvatarType.FEMALE;

        }
        ShowConfirmPopup();
    }

    private void ShowConfirmPopup()
    {
        UIManager.NotificationManager.ShowNotification(
               "TwoOptionsTitleUINotification",
               -1,
               false,
               "Character Customisation",
               "Do you want to customise your assistant's character?",
               null,
               new string[] { "No", "Yes" },
               new string[] { "No", "Yes" },
               new UnityAction[]
                    {
                        null,
                        () =>
                        {
                           ScreenManager.Instance.Show("CustomAvatarScreen");  
                        }
                    }
           );
    }
}
