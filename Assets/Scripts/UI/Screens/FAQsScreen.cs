﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FAQsScreen : MonoBehaviour
{
    [SerializeField] private ScreenController _screenController;


    private void Awake()
    {
        _screenController.OnShowStarted += ShowStarted;

    }

    private void ShowStarted()
    {
        AppManager.Instance.BottomPanel.SetActive(false);
        AppManager.Instance.ScreenTitle.text = "DISEASE RELATEDS - TOPIC";
    }

}
