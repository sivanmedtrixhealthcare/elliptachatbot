﻿using System;
using System.Collections.Generic;
using App.Data;
//using MaterialUI;
using UnityEngine;
using UnityEngine.UI;

public class TrackerScreen : MonoBehaviour
{
    [SerializeField] private ScreenController _screenController;
    [SerializeField] protected GameObject _scorePanel;
    [SerializeField] protected Text _pointsText;
    [SerializeField] protected Text _questionText;
    [SerializeField] protected GameObject _submitButton;
    [SerializeField] protected TrackerProgressController _progressController;

    [Header("Answers references")] [SerializeField]
    protected Transform _answersContainer;

    [SerializeField] protected GameObject _QAPanel;
    [SerializeField] protected GameObject _ProgressPanel;
     

    [SerializeField] protected ToggleGroup _toggleGroup;
    [SerializeField] protected OptionToggle _answerOptionPrefab;

    protected TrackerManager.TrackerType _trackerType;
    protected QuestionBasedTrackerData _trackerData;
    protected List<OptionToggle> _optionsTogglesList = new List<OptionToggle>();

    //[SerializeField] protected ObjectPool objPool;

    private void Awake()
    {
        //Debug.Log("Coming to tracker screen Awake....");
        _screenController.OnShowStarted += ShowStarted;

    }

    private void ShowStarted()
    {
        //Debug.Log("Coming to tracker screen ShowStarted....");

        AppManager.Instance.BottomPanel.SetActive(false);
        AppManager.Instance.SwitchAvatar.SetActive(false);
        AppManager.Instance.Backbutton.SetActive(true);


        if (AppManager.Instance.currentAppMode == AppMode.SAA)
             AppManager.Instance.ScreenTitle.text = "ASTHMA CONTROL TEST";
        else if (AppManager.Instance.currentAppMode == AppMode.COPD)
            AppManager.Instance.ScreenTitle.text = "COPD CONTROL TEST";
    }

    public virtual void Start()
    {
        //Debug.Log("Coming to tracker screen start....");

        _progressController.OnBackClicked += SetPreviousQuestion;
    }

    public virtual void StartTracker()
    {
        //Debug.Log("Coming to Tracker screen start tracker ....");
        // hide score panel
        _scorePanel.SetActive(false);

        // reset question index and show 1st question
        _trackerData.ResetQuestionIndex();
        InitiateQuestion(_trackerData.GetQuestion());
    }

    public void NextQuestion(int answerIndex)
    {
        // Debug.Log("Next question. Answer index: " + answerIndex);
        var question = _trackerData.GetQuestion();

        try
        {
            QuestionBasedTrackerData.QuestionData nextQuestion = _trackerData.SetAnswer(question, answerIndex);
            InitiateQuestion(nextQuestion);
        }
        catch (Exception ex)
        {
            Debug.LogWarning(
                "Exception caught trying to get new question. Seems there is no more questions for this data. Ex: " +
                ex.Message);

            TryCompleteTracker();
        }
    }

    protected void InitiateQuestion(QuestionBasedTrackerData.QuestionData question)
    {
        _questionText.text = question.question;

        // update progress UI
        _progressController.UpdateProgress(_trackerData.GetCurrentQuestionIndex(), _trackerData.questionDataList.Count);

        // disable all existing toggle options
        for (int i = 0; i < _optionsTogglesList.Count; i++)
        {
            _optionsTogglesList[i].gameObject.SetActive(false);
        }
        Debug.Log("Coming inside Initiate Question : "+ question.answersOption.Length);

        for (int i = 0; i < question.answersOption.Length; i++)
        {
            OptionToggle optionToggle;

            if (_optionsTogglesList.Count < question.answersOption.Length)
            {
                 GameObject gObject = Instantiate(_answerOptionPrefab.gameObject, Vector3.zero, Quaternion.identity, _answersContainer);
                //_answerOptionPrefab.transform.SetParent();
                optionToggle = gObject.GetComponent<OptionToggle>();
                //optionToggle = objPool.Instantiate(_answerOptionPrefab, Vector3.zero, Quaternion.identity, _answersContainer).GetComponent<OptionToggle>();
                 optionToggle.toggle.group = _toggleGroup;
                _optionsTogglesList.Add(optionToggle);
            }
            // if there are too many answers toggles already
            else
            {
                optionToggle = _optionsTogglesList[i];
            }

            // enable each used toggle
            optionToggle.gameObject.SetActive(true);

            // hide submit button
            // _submitButton.SetActive(false);

            // update toggle
            _toggleGroup.SetAllTogglesOff();

            optionToggle.SetAnswer(question.answersOption[i].description);
        }
    }

    protected virtual void TryCompleteTracker()
    {
        Debug.Log("Score: " + _trackerData.GetScore());
        _QAPanel.gameObject.SetActive(false);
        _ProgressPanel.transform.gameObject.SetActive(false);
        _scorePanel.SetActive(true);
        _pointsText.text = _trackerData.GetScore().ToString();
    }

    public virtual void RedoTest()
    {
        // hide score panel
        _scorePanel.SetActive(false);

        // start test again
        StartTracker();
    }

    public virtual void SubmitResults()
    {
        // save progress
        QuestionBasedTrackerData originalData = TrackerManager.GetData(DateTime.Today, _trackerType);

        originalData.SetAnswers(_trackerData.GetAnswers());
        TrackerManager.UpdateEntry(DateTime.Today, originalData);
    }
    
    /// <summary>
    /// Submit answer by clicking the button.
    /// </summary>
    public virtual void SubmitAnswer()
    {
        for (int i = 0; i < _optionsTogglesList.Count; i++)
        {
            if (_optionsTogglesList[i].IsOn)
            {
                NextQuestion(i);
                return;
            }
        }
    }

    public void SetPreviousQuestion()
    {
        int currentQuestionIndex = _trackerData.GetCurrentQuestionIndex();

        // get previous question data
        QuestionBasedTrackerData.QuestionData prevQuestion =
            _trackerData.questionDataList[currentQuestionIndex - 1];

        Debug.Log($"currentQuestionIndex: {currentQuestionIndex}, was previous question skipped? : " +
                  _trackerData.WasQuestionSkipped(prevQuestion));

        int qIndex;

        // if that was a skip able question, check, was it skipped or not
        if (_trackerData.WasQuestionSkipped(prevQuestion))
        {
            // jump over 1 question back
            qIndex = currentQuestionIndex - 2;
        }
        else
        {
            qIndex = currentQuestionIndex - 1;
        }

        // update questions & answers view
        InitiateQuestion(_trackerData.GetQuestion(qIndex));

        // set the previously selected option
        _optionsTogglesList[_trackerData.GetAnswerOption(qIndex)].SetValue(true);

        // _submitButton.SetActive(true);
    }
}