﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
 
 
public class SpriteSheetManager : MonoSingleton<SpriteSheetManager>
{
	public Dictionary<string, Sprite> _asthmaSprites;
    public Dictionary<string, Sprite> _copdSprites;
    public Dictionary<string, Sprite> _commonSprites;

    void Awake()
    {
        LoadAsthmaDictionary();
        LoadCOPDDictionary();
        LoadCommonDictionary();
    }


    private void LoadAsthmaDictionary()
	{
		Sprite[] SpritesData = Resources.LoadAll<Sprite>("Spritesheets/AsthmaSpritesheet_1");
		_asthmaSprites = new Dictionary<string, Sprite>();

		for (int i = 0; i < SpritesData.Length; i++)
		{
			_asthmaSprites.Add(SpritesData[i].name, SpritesData[i]);
		}
        //Debug.Log("Asthma sprites count :>>>>"+ _asthmaSprites.Count);
	}
	
	private void LoadCOPDDictionary()
	{
		Sprite[] SpritesData = Resources.LoadAll<Sprite>("Spritesheets/COPDSpritesheet_1");
		_copdSprites = new Dictionary<string, Sprite>();

		for (int i = 0; i < SpritesData.Length; i++)
		{
			_copdSprites.Add(SpritesData[i].name, SpritesData[i]);
		}
        //Debug.Log("LoadCOPDDictionary sprites count :>>>>" + _copdSprites.Count);
    }

	private void LoadCommonDictionary()
	{
		Sprite[] SpritesData = Resources.LoadAll<Sprite>("Spritesheets/CommonSpritesheet_1");
		_commonSprites = new Dictionary<string, Sprite>();

		for (int i = 0; i < SpritesData.Length; i++)
		{
			_commonSprites.Add(SpritesData[i].name, SpritesData[i]);
		}
        //Debug.Log("LoadCommonDictionary sprites count :>>>>" + _commonSprites.Count);

    }

    /*public Sprite GetAsthmaSpriteByName(string name)
	{
		
	}


	public Sprite GetCOPDSpriteByName(string name)
	{
		
	}*/


    public Sprite GetSpriteFromAtlas(AppMode appMode, string name)
    {
         if(appMode == AppMode.SAA)
         {
            //Debug.Log("ASTHMA SPRITES COUNT :"+ _asthmaSprites.Count);
            if (_asthmaSprites.ContainsKey(name))
                return _asthmaSprites[name];
            else
                return null;
         }else if(appMode == AppMode.COPD)
         {

            if (_copdSprites.ContainsKey(name))
                return _copdSprites[name];
            else
                return null;
         }
        return null;
    }


	public Sprite GetCommonSpriteByName(string name)
	{
		if (_commonSprites.ContainsKey(name))
			return _commonSprites[name];
		else
			return null;
	}

}
