﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BayatGames.SaveGamePro;

public class PlayerData : MonoBehaviour
{
    [SerializeField] private InputField AgeTxt;
    [SerializeField] private InputField WeightTxt;
    [SerializeField] private InputField AllergicTxt;

    public Dropdown GenderDropDown;
    [SerializeField] private ScreenController _screenController;
    [SerializeField] private RawImage UserProfilePhoto;
    [SerializeField] private GameObject ChoosePhotoOption;
    private string selectedImagePath;

    [SerializeField] private GameObject tempPicIcon;


    private Dictionary<string, string> profileDetails = new Dictionary<string, string>();
    private const string USER_PROFILE_IDENTIFIER = "UserProfile.dat";


    // Start is called before the first frame update
    private void Awake()
    {
        _screenController.OnShowStarted += ShowStarted;

    }

    private void ShowStarted()
    {
        AppManager.Instance.BottomPanel.SetActive(false);
        AppManager.Instance.ScreenTitle.text = "SIGN UP";
    }

    private void Start()
    {
        if (SaveGame.Exists(USER_PROFILE_IDENTIFIER))
        {
            profileDetails = SaveGame.Load<Dictionary<string, string>>(USER_PROFILE_IDENTIFIER);
            //Debug.Log("LOADED USER PROFILE DATA :>" + USER_PROFILE_IDENTIFIER);

            foreach (KeyValuePair<string, string> dictValue in profileDetails)
            {
                //Debug.Log("RETREIVED KEY AND VALUE ::::::::" + dictValue.Key + " VALUE: " + dictValue.Value); 
                ShowSavedUserData(dictValue.Key, dictValue.Value);
            }
        }
    }

    private void ShowSavedUserData(string key, string value)
    {
        switch(key)
        {
            case "Age":
                AgeTxt.text = value;
                AppManager.Instance.UserAge = value;
                break;
            case "Gender":
                GenderDropDown.value = (value == "Male" ? 0 : 1);
                AppManager.Instance.UserGender = value;
                break;
            case "Weight":
                WeightTxt.text = value;
                break;
            case "Allergy":
                AllergicTxt.text = value;
                break;

            case "ProfileDP":
                selectedImagePath = value;
                LoadSavedDP(selectedImagePath, 512);
                break;

        }
    }

      

    public void SaveData()
    {
        if (AgeTxt.text == "" || GenderDropDown.captionText.text == "" || WeightTxt.text == "" || AllergicTxt.text == "")
            return;


        if (!profileDetails.ContainsKey("Age"))
        {
            profileDetails.Add("Age", AgeTxt.text);
            AppManager.Instance.UserAge = AgeTxt.text;
        }
        else
        {
            profileDetails["Age"] = AgeTxt.text;
        }

        if (!profileDetails.ContainsKey("Gender"))
        {
            profileDetails.Add("Gender", GenderDropDown.captionText.text);
            AppManager.Instance.UserGender = GenderDropDown.captionText.text;
        }
        else
        {
            profileDetails["Gender"] = GenderDropDown.captionText.text;
        }

        if (!profileDetails.ContainsKey("Weight"))
        {
            profileDetails.Add("Weight", WeightTxt.text);
        }
        else
        { 
            profileDetails["Weight"] = WeightTxt.text;
        }

        if (!profileDetails.ContainsKey("Allergy"))
        {
            profileDetails.Add("Allergy", AllergicTxt.text);
        }
        else
        {
            profileDetails["Allergy"] = AllergicTxt.text;
        }


        if (!profileDetails.ContainsKey("ProfileDP"))
        {
            profileDetails.Add("ProfileDP", selectedImagePath);
        }
        else
        {
            profileDetails["ProfileDP"] = selectedImagePath;
        }


        SaveGame.Save(USER_PROFILE_IDENTIFIER, profileDetails);

        if (SaveGame.Exists(AppManager.CUSTOM_CHARACTER_IDENTIFIER))
        {
            ScreenManager.Instance.Show("ConditionMenuScreen");
        }else
        {
            ScreenManager.Instance.Show("AvatarSelectionScreen");
        }
    }
     
    public void OnTakePhotoClicked()
	{
        //ChoosePhotoOption.SetActive(false);
    }

    public void OnUploadPhotoClicked()
    {
        PickImage(2048);
        //UserProfilePhoto = testTexture.texture;
    }

    private void PickImage(int maxSize)
    {
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            Debug.Log("Image path: " + path);
            if (path != null)
            {
                selectedImagePath = path.ToString();
                // Create Texture from selected image
                Texture2D texture = NativeGallery.LoadImageAtPath(path, maxSize);
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }

                // Assign texture to a temporary quad and destroy it after 5 seconds
                /*GameObject quad = GameObject.CreatePrimitive(PrimitiveType.Quad);
                quad.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2.5f;
                quad.transform.forward = Camera.main.transform.forward;
                quad.transform.localScale = new Vector3(1f, texture.height / (float)texture.width, 1f);

                Material material = quad.GetComponent<Renderer>().material;
                if (!material.shader.isSupported) // happens when Standard shader is not included in the build
                    material.shader = Shader.Find("Legacy Shaders/Diffuse"); 

                material.mainTexture = texture;*/
                tempPicIcon.SetActive(false);
                UserProfilePhoto.texture = texture;
                (UserProfilePhoto.texture as Texture2D).Apply();
                AppManager.Instance.UserProfilePhoto = texture;


            }
        }, "Select a PNG image", "image/png");

        Debug.Log("Permission result: " + permission);
    }

    private void LoadSavedDP(string dpPath, int maxSize)
    {
        if (dpPath != null)
        {
            Debug.Log("Path is not null" + dpPath);
            // Create Texture from selected image
            Texture2D texture = NativeGallery.LoadImageAtPath(dpPath, maxSize);
            if (texture == null)
            {
                Debug.Log("Couldn't load texture from " + dpPath);
                return;
            }
            tempPicIcon.SetActive(false);
            UserProfilePhoto.texture = texture;

        }
    }

}
