﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VoxelBusters.NativePlugins;


public class SharingScore : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }
    public void ShareViaShareSheet()
    {
        // Create new instance and populate fields
        ShareSheet _shareSheet = new ShareSheet();
        _shareSheet.Text = "This is a test message.";

        // On iPad, popover view is used to show share sheet. So we need to set its position
        NPBinding.UI.SetPopoverPointAtLastTouchPosition();

        // Show composer
        NPBinding.Sharing.ShowView(_shareSheet, OnFinishedSharing);
    }

    private void OnFinishedSharing(eShareResult _result)
    {
        // Insert your code
        Debug.Log("On Finished Sharing..");
    }

    
 }

 
