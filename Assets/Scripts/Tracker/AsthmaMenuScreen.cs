﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsthmaMenuScreen : MonoBehaviour
{
    [SerializeField] private ScreenController _screenController;

    // Start is called before the first frame update
    void Awake()
    {
        _screenController.OnShowStarted += ShowStarted;
        //_screenController.OnHideStarted += HideStarted;
        AppManager.OnAsthmaModeChanged += OnAsthmaModeChanged;
    }
     

    private void OnAsthmaModeChanged()
    {
        Debug.Log("CURRENT APP MODE IN ASTHMA :>>>>>>>"+AppManager.Instance.currentAppMode);
        ScreenManager.Instance.Show("COPDTrackerMenu");  
    }


    private void ShowStarted()
    {

        AppManager.Instance.BottomPanel.GetComponent<BottomPanelController>().SetButtonSprites();
        AppManager.Instance.Backbutton.SetActive(true);
        AppManager.Instance.BottomPanel.SetActive(true);
    }

}
