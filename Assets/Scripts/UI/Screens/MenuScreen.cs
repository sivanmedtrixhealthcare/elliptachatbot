﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScreen : MonoBehaviour
{
    [SerializeField] private ScreenController _screenController;
    [SerializeField] private RawImage UserProfilePhoto;
    [SerializeField] private GameObject TempProfilePhoto;
    [SerializeField] private Text GenderAge;

    private void Awake()
    {
        _screenController.OnShowStarted += ShowStarted;
    }

    private void ShowStarted()
    {
        AppManager.Instance.BottomPanel.SetActive(false);
        UserProfilePhoto.texture = AppManager.Instance.UserProfilePhoto;
        TempProfilePhoto.SetActive(false);
        GenderAge.text = AppManager.Instance.UserAge+","+AppManager.Instance.UserGender;
        Debug.Log(AppManager.Instance.UserAge + ": Debug for gender age text :" + AppManager.Instance.UserGender);
    }

    public void OnLogOutClicked()
    {
        Application.Quit();
    }

}
