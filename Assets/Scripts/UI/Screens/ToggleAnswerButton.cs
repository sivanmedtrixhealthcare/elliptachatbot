﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleAnswerButton : MonoBehaviour
{
    //[SerializeField] private GameObject questionBar;
    [SerializeField] private GameObject answerBar;
    [SerializeField] private GameObject DownArrow;
    [SerializeField] private GameObject UpArrow;


    public void OnShowAnswerBox(bool isValueChanged)
    {
        if(isValueChanged)
        {
            answerBar.SetActive(true);
            DownArrow.SetActive(false);
            UpArrow.SetActive(true);

        }
        else
        {
            answerBar.SetActive(false);
            DownArrow.SetActive(true);
            UpArrow.SetActive(false);
        }

    }
}
