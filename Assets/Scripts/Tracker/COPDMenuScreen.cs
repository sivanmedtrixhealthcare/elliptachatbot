﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class COPDMenuScreen : MonoBehaviour
{
    [SerializeField] private ScreenController _screenController;

    // Start is called before the first frame update
    void Awake()
    {
        _screenController.OnShowStarted += ShowStarted;
        //_screenController.OnHideStarted += HideStarted;
        AppManager.OnCOPDModeChanged += OnCOPDModeChanged;
    }

    private void OnCOPDModeChanged()
    {
        Debug.Log("CURRENT APP MODE IN COPD :>>>>>>> " + AppManager.Instance.currentAppMode);
        ScreenManager.Instance.Show("AsthmaTrackerMenu");
    }

    private void ShowStarted()
    {

        AppManager.Instance.BottomPanel.GetComponent<BottomPanelController>().SetButtonSprites();
        AppManager.Instance.Backbutton.SetActive(true);
        AppManager.Instance.BottomPanel.SetActive(true);
    }


}
