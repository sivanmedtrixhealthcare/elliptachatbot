﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
[Flags]
[Serializable]
public enum AppMode
{
    SAA = 1,
    COPD = 2
}


[Serializable]
public enum AvatarType
{
    MALE = 1,
    FEMALE = 2
}

public class AppManager : MonoSingleton<AppManager>
{
    public AppMode currentAppMode;
    public AvatarType currentAvatarType;

    public static Action<AppMode> OnModeChange;
    public static Action OnAsthmaModeChanged;
    public static Action OnCOPDModeChanged;
    public Text menuSwitchText;
    public GameObject BottomPanel;
    public GameObject CustomCharBottomPanel;
    public GameObject ChatbotBG;
    public GameObject Backbutton;

    //public GameObject[] Hairs;
    public Text ScreenTitle;
    public Camera modelCamera;

    public const string CUSTOM_CHARACTER_IDENTIFIER = "CustomCharacterData.dat";
    public Texture2D UserProfilePhoto;
    public GameObject SwitchAvatar;
    public string UserGender;
    public string UserAge;


    public void OnSwitchMode()
    {
        //Debug.Log("OnSwithch Mode in BottomPanelController is "+ appMode);
        OnModeChange?.Invoke(currentAppMode);

        if (currentAppMode == AppMode.SAA)
        {
            currentAppMode = AppMode.COPD;
            menuSwitchText.text = "SWITCH TO COPD";
            OnAsthmaModeChanged.Invoke();
        }
        else if (currentAppMode == AppMode.COPD)
        {
            currentAppMode = AppMode.SAA;
            menuSwitchText.text = "SWITCH TO SAA";
            OnCOPDModeChanged.Invoke();
        }
    }


}
