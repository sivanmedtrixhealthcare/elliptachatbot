﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatbotController : MonoBehaviour
{

    [SerializeField] private Watson_newServices watsonNewServices;
    [SerializeField] private InputField inputText;
    [SerializeField] private Text questionText;
    [SerializeField] private Text answerText;
    [SerializeField] private GameObject questionPanel;

    //[SerializeField] private RenderTexture CharacterRenderTexture;

    [SerializeField] private RectTransform CharacterRenderTexture;
    [SerializeField] private ScreenController _screenController;

    void Awake()
    {
        _screenController.OnShowStarted += ShowStarted;
        _screenController.OnHideStarted += HideStarted;

    }
    private void ShowStarted()
    {
        AppManager.Instance.BottomPanel.SetActive(true);
        AppManager.Instance.SwitchAvatar.SetActive(false);
        CharacterRenderTexture.transform.gameObject.SetActive(true);
        //AppManager.Instance.ChatbotBG.SetActive(true);

        if (AppManager.Instance.currentAvatarType == AvatarType.MALE)
        {
            AppManager.Instance.modelCamera.transform.position = new Vector3(0, 1.1f, -4f);
            
        }
        else if (AppManager.Instance.currentAvatarType == AvatarType.FEMALE)
        {
            AppManager.Instance.modelCamera.transform.position = new Vector3(2, 1.1f, -4f);

        }
        AppManager.Instance.modelCamera.orthographicSize = 0.8f;
        AppManager.Instance.BottomPanel.GetComponent<BottomPanelController>().SetButtonSprites();
    }

    private void HideStarted()
    {
        //AppManager.Instance.ChatbotBG.SetActive(false);
        CharacterRenderTexture.transform.gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
 
        watsonNewServices.OnOutputRetrieved += OnShowingAnswer;
        watsonNewServices.StartAllWatsonServices();
    }

    public void OnQuestionToBot()
    {
        //Debug.Log("Came to On Question to Bot.");

        questionText.text = inputText.text;
        watsonNewServices.QuestionToChatbot(inputText.text);
        inputText.text = "";
    }

    public void OnStartRecording()
    {
        //Debug.Log("Came to OnStartRecording.");
        watsonNewServices.StopRecording();
        watsonNewServices.StartRecording();

    }

    private void OnShowingAnswer()
    {
        questionPanel.gameObject.SetActive(true);
        answerText.text = watsonNewServices.AnswerForQuestion;
        //Debug.Log("Result ::::>>>>>>>");

    }

    public void OnCloseResultPanel()
    {
        questionPanel.gameObject.SetActive(false);
    }
}
