﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisclaimerScreen : MonoBehaviour
{
    [SerializeField] private ScreenController _screenController;

    // Start is called before the first frame update
    private void Awake()
    {
        _screenController.OnShowStarted += ShowStarted;
        
    }

    private void ShowStarted()
    {
        AppManager.Instance.BottomPanel.SetActive(false);

    }
}