﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottomPanelController : MonoBehaviour
{
    public Button HomeButton;
    public Button InhalerButton;
    public Button SwitchButton;
    public Button TrackerButton;
    public Button CalendarButton;
    [SerializeField] private Text AppModeTxt;

    // Start is called before the first frame update
    void Start()
    {
        //AppManager.OnModeChange += OnSwitchMode;
        SetButtonSprites();
    }
     

    public void OnTrackerButtonClicked()
    {
        if (AppManager.Instance.currentAppMode == AppMode.SAA)
        {
            ScreenManager.Instance.Show("AsthmaTrackerMenu");
        }
        else if (AppManager.Instance.currentAppMode == AppMode.COPD)
        {
            ScreenManager.Instance.Show("COPDTrackerMenu");
        }
    }

    public void OnSwitchMode()
    {
        //Debug.Log("OnSwithch Mode in BottomPanelController is "+ appMode);
        if (AppManager.Instance.currentAppMode == AppMode.SAA)
        {
            //AppManager.Instance.currentAppMode = AppMode.COPD;
            AppModeTxt.text = "COPD";

        }
        else if (AppManager.Instance.currentAppMode == AppMode.COPD)
        {
            //AppManager.Instance.currentAppMode = AppMode.SAA;
            AppModeTxt.text = "ASTHMA";

        }
        AppManager.Instance.OnSwitchMode();
        SetButtonSprites();
    }


    public void SetButtonSprites()
    {
        HomeButton.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas(AppManager.Instance.currentAppMode, "home_active@3x");
        InhalerButton.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas(AppManager.Instance.currentAppMode, "inhaler_active@3x");
        SwitchButton.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas(AppManager.Instance.currentAppMode, "switch_active@3x");
        TrackerButton.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas(AppManager.Instance.currentAppMode, "tracker_active@3x");
        CalendarButton.GetComponent<Image>().sprite = SpriteSheetManager.Instance.GetSpriteFromAtlas(AppMode.SAA, "sidemenu_calendar_active@3x");

        if (AppManager.Instance.currentAppMode == AppMode.SAA)
        {
            AppModeTxt.text = "ASTHMA";
        }
        else if (AppManager.Instance.currentAppMode == AppMode.COPD)
        {
            AppModeTxt.text = "COPD";
        }
    }

}
