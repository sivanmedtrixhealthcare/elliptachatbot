﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WelcomeScreen : MonoBehaviour
{
    public void OnGetStarted()
    {
        if (PlayerPrefs.HasKey("AccessCode"))
        {
            ScreenManager.Instance.Show("ConditionMenuScreen");

        }else
        {
            ScreenManager.Instance.Show("LoginScreen");
        }
    }
}
